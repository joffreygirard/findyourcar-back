import { AbstractMapper } from '../common/abstract.mapper';
import { IUser, IUserDto } from './user.model';

export class UserMapper extends AbstractMapper<IUser, IUserDto> {

    modelToDto(model: IUser): IUserDto {
        return {
            id: model._id,
            login: model.login,
            password: model.password,
            firstname: model.firstname,
            lastname: model.lastname,
            birthdate: model.birthdate,
            role: model.role
        };
    }

    dtoToModel(dto: IUserDto): IUser {
        return {
            login: dto.login,
            password: dto.password,
            firstname: dto.firstname,
            lastname: dto.lastname,
            birthdate: new Date(dto.birthdate),
            role: dto.role
        };
    }
}

export const userMapper = new UserMapper();
