import { AbstractController } from '../common/abstract.controller';
import { AbstractService } from '../common/abstract.service';
import { IBrand, IBrandDto } from './brand.model';
import { brandService } from './brand.service';

class BrandController extends AbstractController<IBrand, IBrandDto> {
    protected service = brandService;
}

export const brandController = new BrandController();
