import { AbstractService } from '../common/abstract.service';
import { brandMapper } from './brand.mapper';
import { IBrand, IBrandDto } from './brand.model';
import { brandRepository } from './brand.repository';

class BrandService extends AbstractService<IBrand, IBrandDto> {
    protected repository = brandRepository;
    protected mapper = brandMapper;
}

export const brandService = new BrandService();
