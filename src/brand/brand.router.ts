import { AbstractRouter } from '../common/abstract.router';
import { IBrand, IBrandDto } from './brand.model';
import { routeParamIdMiddleware } from '../middleware/route-param-id.middleware';
import { authenticationMiddleware } from '../middleware/authentication.middleware';
import { authorizationMiddleware } from '../middleware/authorization.middleware';
import { UserRole } from '../user/user.model';
import { brandController } from './brand.controller';

class BrandRouter extends AbstractRouter<IBrand, IBrandDto> {
    protected controller = brandController;

    protected configure() {
        this.router.get('/', (req, res, next) => this.controller.findAll(req, res, next));

        this.router.get('/:id',
            routeParamIdMiddleware,
            (req, res, next) => this.controller.get(req, res, next));

        this.router.post('/',
            authenticationMiddleware,
            authorizationMiddleware(UserRole.admin),
            (req, res, next) => this.controller.create(req, res, next));

        this.router.put('/:id',
            routeParamIdMiddleware,
            authenticationMiddleware,
            authorizationMiddleware(UserRole.admin),
            (req, res, next) => this.controller.update(req, res, next));

        this.router.delete('/:id',
            routeParamIdMiddleware,
            authenticationMiddleware,
            authorizationMiddleware(UserRole.admin),
            (req, res, next) => this.controller.remove(req, res, next));
    }

}

export const brandRouter = new BrandRouter().router;
