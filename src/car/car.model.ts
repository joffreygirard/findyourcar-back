import { IDto, IModel } from '../common/abstract.model';

export interface ICar extends IModel {
    brand: string;
    model: string;
    year: number;
    image: string;
    price: number;
    horsepower: string;
}

export interface ICarDto extends IDto {
    brand: string;
    model: string;
    year: number;
    image: string;
    price: number;
    horsepower: string;
}
