import { IDto, IModel } from '../common/abstract.model';

export interface IBrand extends IModel {
    name: string;
}

export interface IBrandDto extends IDto {
    name: string;
}
