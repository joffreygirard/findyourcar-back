import { AbstractMapper } from '../common/abstract.mapper';
import { IBrand, IBrandDto } from './brand.model';

export class BrandMapper extends AbstractMapper<IBrand, IBrandDto> {

    modelToDto(model: IBrand): IBrandDto {
        return {
            id: model._id,
            name: model.name
        };
    }

    dtoToModel(dto: IBrandDto): IBrand {
        return {
            name: dto.name
        };
    }
}

export const brandMapper = new BrandMapper();
