import { AbstractRepository } from '../common/abstract.repository';
import { IGarage } from './garage.model';
import {PrimaryKeyError} from '../common/error/repository-error.model';

class GarageRepository extends AbstractRepository<IGarage> {

    findByUser(id: string): Promise<IGarage> {
        return this.db.collection(this.collection).findOne({user_id:id})
            .then(item => {
                if (item !== null) {
                    return item;
                } else {
                    throw new PrimaryKeyError();
                }
            });
    }

}

const collection = 'garage';
export const garageRepository = new GarageRepository(collection);
