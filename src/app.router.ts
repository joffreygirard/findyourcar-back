import express from 'express';
import { userRouter } from "./user/user.router";
import { authRouter } from "./auth/auth.router";
import { garageRouter } from "./garage/garage.router";
import { brandRouter } from "./brand/brand.router";
import { carRouter } from "./car/car.router";
import { routeNotFoundMiddleware } from "./middleware/route-not-found.middleware";

export const router = express.Router();
router.use('/users', userRouter);
router.use('/auth', authRouter);
router.use('/garages', garageRouter);
router.use('/brands', brandRouter);
router.use('/cars', carRouter);
router.use(routeNotFoundMiddleware);
