import { NextFunction, Request, Response } from 'express';
import { logger } from '../common/logger.config';

export const logMiddleware = (request: Request, response: Response, next: NextFunction): void => {
    logger.info(`METHOD : ${request.method} `
        + `| PATH : ${request.url} `
        + `| CONTENT-TYPE : ${request.headers["content-type"]}`);
    next();
}
