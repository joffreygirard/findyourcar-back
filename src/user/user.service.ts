import { AbstractService } from '../common/abstract.service';
import { userMapper } from './user.mapper';
import { IUser, IUserDto } from './user.model';
import { userRepository } from './user.repository';

class UserService extends AbstractService<IUser, IUserDto> {
    protected repository = userRepository;
    protected mapper = userMapper;
}

export const userService = new UserService();
