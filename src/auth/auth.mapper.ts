import { AbstractMapper } from '../common/abstract.mapper';
import { IUser } from '../user/user.model';
import { IAuthMeDto } from './auth.model';

class AuthMeMapper extends AbstractMapper<IUser, IAuthMeDto> {
    dtoToModel(dto: IAuthMeDto): IUser {
        return undefined;
    }

    modelToDto(model: IUser): IAuthMeDto {
        return {
            id: model._id,
            login: model.login,
            firstname: model.firstname,
            lastname: model.lastname,
            birthdate: model.birthdate,
            role: model.role,
        };
    }
}

export const authMeMapper = new AuthMeMapper();
