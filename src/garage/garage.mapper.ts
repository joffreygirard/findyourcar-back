import { AbstractMapper } from '../common/abstract.mapper';
import { IGarage, IGarageDto } from './garage.model';

export class GarageMapper extends AbstractMapper<IGarage, IGarageDto> {

    modelToDto(model: IGarage): IGarageDto {
        return {
            id: model._id,
            user_id: model.user_id,
            cars: model.cars
        };
    }

    dtoToModel(dto: IGarageDto): IGarage {
        return {
            user_id: dto.user_id,
            cars: dto.cars
        };
    }
}

export const garageMapper = new GarageMapper();
