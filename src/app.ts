import express from 'express';
import cors from 'cors';
import { router } from './app.router';
import { logMiddleware } from "./middleware/log.middleware";
import { errorMiddleware } from "./middleware/error.middleware";
import helmet from 'helmet';
import compression from 'compression';

export const app = express();

app.use(helmet());
app.use(compression())
app.use(cors());
app.use(logMiddleware);
app.use(express.json());
app.use('/api', router);
//app.get('*.*', express.static('static'));
//app.get('*', (req, res) => {res.sendFile(__dirname + '/static/index.html')})
app.use(errorMiddleware);
