import { UserRole } from '../user/user.model';
import { IDto } from '../common/abstract.model';

export interface IAuthCredentials {
    login: string;
    password: string;
}

export interface IAuthToken {
    token: string;
}

export interface IAuthMeDto extends IDto {
    login: string;
    firstname: string;
    lastname: string;
    birthdate: string;
    role: UserRole;
}
