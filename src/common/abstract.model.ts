export interface IModel {
    _id?: string;
}

export interface IDto {
    id?: string;
}
