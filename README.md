# FindYourCarBack

Projet réalisé par Joffrey Girard, Delabarre Aymeric et Parlier Valentin.

## Sujet

Notre avons décidé de réaliser un garage connecté avec la possibilité d'y ajouter les dernières voitures à la mode dès lors que l'utilisateur est connecté à notre application. 

## Base de données

Notre base de données contient 4 collections :

### USER
| name | type |
| ------ | ------ |
| id | ObjectId() |
| login | String |
| password | String |
| firstname | String |
| lastname | String |
| role | String |
| birthdate | $date |

### GARAGE
| name | type |
| ------ | ------ |
| id | ObjectId() |
| user_id | String |
| cars | Array |

### CAR
| name | type |
| ------ | ------ |
| id | ObjectId() |
| model | String |
| year | Number |
| image | String |
| price | Number |
| horsepower | String |
| brand | String |

### BRAND
| name | type |
| ------ | ------ |
| id | ObjectId() |
| name | String |

Nous avons utilisé une base de données MongoDB.
La configuration de la connection à la base de données s'effectue dans le fichier abstract.repository.ts
Il faut remplacer le contenu de la constante "url" dans la fonction "connect()" par l'url vers votre base de données.

## Structure générale du projet

Fichiers de configuration qui permettent de configurer le serveur node.js et la base de l'application.

Fichiers d'accès aux ressources de la base de données.

Fichiers communs :
- Classes abstraites pour la connexion à la base de données
- Service pour générer le token d'authentification
- Configuration du module pour générer les logs
- Middleware

## Couches techniques - Accès aux ressources

1. Router : Redirige la requête HTTP vers la bonne route
2. Controleur : Créé la réponse HTTP avec un retour de données
3. Service : Lien entre controller et repository + formatage des données
4. Repository : Connexion directe avec la BDD
5. Mapper & Model : Formatage et typage des données

## Couches techniques - Middleware

1. Authentication : Confirme l’identité du client (connexion)
2. Authorization : Vérification des droits d’accès aux ressources (rôles)
3. Error : Retourne un code d’erreur lisible par le client
4. Log : Log les infos de chaque requête
5. Route not found : Catch les requêtes vers des routes inexistantes
6. Route param ID : Vérifie le format du paramètre ID d’une requête

## Mécanisme d'authentication

Authentification avec JSON Web Token

1. Le client se log sur le front avec son login et son mot de passe (requête HTTP vers la route /api/auth/login avec en body ses credentials)
2. Le Back vérifie l'existance de l'utilisateur en BDD
3. Le Back génère un token avec la fonction sign de JWT et le renvoi au client
4. Le client stock le token dans une BDD localStorage (sur le navigateur)
5. Pour chaque requête, le client ajoute en en-tête Authorization: Bearer token
6. Le Back vérifie la validité du token

## Installer le projet

npm install

## Lancement du projet

npm start



