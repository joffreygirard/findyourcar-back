import { AbstractController } from '../common/abstract.controller';
import { IUser, IUserDto } from './user.model';
import { userService } from './user.service';

class UserController extends AbstractController<IUser, IUserDto> {
    protected service = userService;
}

export const userController = new UserController();
