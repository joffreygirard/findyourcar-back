import { IDto, IModel } from '../common/abstract.model';

export enum UserRole {
    user = 'USER',
    admin = 'ADMIN'
}

export interface IUser extends IModel {
    login: string;
    password: string;
    firstname: string;
    lastname: string;
    birthdate: any;
    role: UserRole;
}

export interface IUserDto extends IDto {
    login: string;
    password: string;
    firstname: string;
    lastname: string;
    birthdate: string;
    role: UserRole;
}
