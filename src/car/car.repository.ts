import { AbstractRepository } from '../common/abstract.repository';
import { ICar } from './car.model';

class CarRepository extends AbstractRepository<ICar> {

}

const collection = 'car';
export const carRepository = new CarRepository(collection);
