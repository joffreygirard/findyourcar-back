import { AbstractRepository } from '../common/abstract.repository';
import { IUser } from './user.model';
import { ModelNotFoundError } from "../common/error/repository-error.model";

class UserRepository extends AbstractRepository<IUser> {

    findByLoginAndPassword(login: string, password: string): Promise<IUser> {
        return this.db.collection(this.collection).findOne({login:login, password:password})
            .then(item => {
                if (item !== null) {
                    return item;
                } else {
                    throw new ModelNotFoundError();
                }
            });
    }

}

const collection = 'user';
export const userRepository = new UserRepository(collection);
