import { AbstractMapper } from '../common/abstract.mapper';
import { ICar, ICarDto } from './car.model';

export class CarMapper extends AbstractMapper<ICar, ICarDto> {

    modelToDto(model: ICar): ICarDto {
        return {
            id: model._id,
            brand: model.brand,
            model: model.model,
            year: model.year,
            image: model.image,
            price: model.price,
            horsepower: model.horsepower
        };
    }

    dtoToModel(dto: ICarDto): ICar {
        return {
            brand: dto.brand,
            model: dto.model,
            year: dto.year,
            image: dto.image,
            price: dto.price,
            horsepower: dto.horsepower
        };
    }
}

export const carMapper = new CarMapper();
