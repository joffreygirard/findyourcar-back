import { AbstractService } from '../common/abstract.service';
import { carMapper } from './car.mapper';
import { ICar, ICarDto } from './car.model';
import { carRepository } from './car.repository';

class CarService extends AbstractService<ICar, ICarDto> {
    protected repository = carRepository;
    protected mapper = carMapper;
}

export const carService = new CarService();
