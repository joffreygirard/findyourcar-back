import { NextFunction, Response } from 'express';
import { ErrorType } from '../common/error/error.model';
import { AuthenticatedRequest } from "./authentication.middleware";
import { UserRole } from "../user/user.model";

export const authorizationMiddleware = (userRole: UserRole) => (req: AuthenticatedRequest, res: Response, next: NextFunction): void => {
    if (req.userToken.role === userRole) {
        next();
    } else {
        next({ type: ErrorType.missingRole, messageParam: userRole });
    }
}
