import { IDto, IModel } from '../common/abstract.model';
import { ICar } from '../car/car.model';

export interface IGarage extends IModel {
    user_id: string;
    cars: ICar[];
}

export interface IGarageDto extends IDto {
    user_id: string;
    cars: ICar[];
}
