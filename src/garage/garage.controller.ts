import { NextFunction, Request, Response } from 'express';
import { AbstractController } from '../common/abstract.controller';
import { IGarage, IGarageDto } from './garage.model';
import { garageService } from './garage.service';

class GarageController extends AbstractController<IGarage, IGarageDto> {
    protected service = garageService;

    findByUser(request: Request, response: Response, next: NextFunction): void {
        const id = request.params.id;
        this.service.findByUser(id)
            .then(dto => response.json(dto))
            .catch(next);
    }
}

export const garageController = new GarageController();
