import { AbstractController } from '../common/abstract.controller';
import { ICar, ICarDto } from './car.model';
import { carService } from './car.service';

class CarController extends AbstractController<ICar, ICarDto> {
    protected service = carService;
}

export const carController = new CarController();
