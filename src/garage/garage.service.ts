import { AbstractService } from '../common/abstract.service';
import { garageMapper } from './garage.mapper';
import { IGarage, IGarageDto } from './garage.model';
import { garageRepository } from './garage.repository';
import { itemErrorHandler } from '../common/error/error.mapper';

class GarageService extends AbstractService<IGarage, IGarageDto> {
    protected repository = garageRepository;
    protected mapper = garageMapper;

    findByUser(id: string): Promise<IGarageDto> {
        return this.repository.findByUser(id)
            .then(model => this.mapper.modelToDto(model))
            .catch(itemErrorHandler(id));
    }
}

export const garageService = new GarageService();
