import { NextFunction, Request, Response, Router } from 'express';
import { authController } from './auth.controller';
import { AuthenticatedRequest, authenticationMiddleware } from '../middleware/authentication.middleware';

export const authRouter = Router();

authRouter.post('/login',
    (req: Request, res: Response, next: NextFunction) => authController.login(req, res, next));

authRouter.get('/me',
    authenticationMiddleware,
    (req: AuthenticatedRequest, res: Response, next: NextFunction) => authController.me(req, res, next));
