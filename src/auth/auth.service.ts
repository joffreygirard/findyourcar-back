import { ErrorType } from '../common/error/error.model';
import { generateToken } from '../common/token.service';
import { userRepository } from '../user/user.repository';
import { IAuthCredentials, IAuthMeDto, IAuthToken } from './auth.model';
import { authMeMapper } from './auth.mapper';

class AuthService {

    login(credentials: IAuthCredentials): Promise<IAuthToken> {
        return userRepository.findByLoginAndPassword(credentials.login, credentials.password)
            .then(user => generateToken({
                id: user._id,
                login: user.login,
                role: user.role
            }))
            .then(token => ({ token }))
            .catch(error => {
                console.error(error);
                return Promise.reject({ type: ErrorType.invalidCredentials });
            });
    }

    getUser(userId: string): Promise<IAuthMeDto> {
        return userRepository.get(userId)
            .then(model => authMeMapper.modelToDto(model));
    }

}

export const authService = new AuthService();
