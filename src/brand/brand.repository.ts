import { AbstractRepository } from '../common/abstract.repository';
import { IBrand } from './brand.model';

class BrandRepository extends AbstractRepository<IBrand> {

}

const collection = 'brand';
export const brandRepository = new BrandRepository(collection);
