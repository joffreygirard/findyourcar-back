import { IModel } from './abstract.model';
import { PrimaryKeyError } from './error/repository-error.model';
import {logger} from './logger.config';

export abstract class AbstractRepository<M extends IModel> {
    protected collection;
    protected ObjectId = require('mongodb').ObjectId;
    protected db;

    public constructor(collection: string) {
        this.collection = collection;
        this.connect().then(database => {
                this.db = database;
            }
        );
    }

    connect() {
        const MongoClient = require('mongodb').MongoClient;
        const url = 'mongodb://localhost:27017';
        const dbName = 'findyourcar';

        return new Promise((resolve, reject) => {
            MongoClient.connect(url, { useUnifiedTopology: true }, function (err, client) {
                if (err) {
                    reject(err);
                } else {
                    logger.info("Connected successfully to database");
                    resolve(client.db(dbName));
                }
            });
        });
    }

    findAll(): Promise<M[]> {
        return this.db.collection(this.collection).find({}).toArray();
    }

    get(id: string): Promise<M> {
        let o_id = new this.ObjectId(id);
        return this.db.collection(this.collection).findOne({_id:o_id})
            .then(item => {
                if (item !== null) {
                    return item;
                } else {
                    throw new PrimaryKeyError();
                }
            });
    }

    create(model: M): Promise<M> {
        return this.db.collection(this.collection).insertOne(model)
            .then(newItem => {
                return newItem.ops[0];
            });
    }

    update(id: string, model: M): Promise<number> {
        let o_id = new this.ObjectId(id);
        return this.db.collection(this.collection).updateOne({_id:o_id},{$set: model})
            .then(updatedItem => {
                if (updatedItem["matchedCount"]) {
                    return updatedItem["matchedCount"];
                } else {
                    throw new PrimaryKeyError();
                }
            });
    }

    remove(id: string): Promise<number> {
        let o_id = new this.ObjectId(id);
        return this.db.collection(this.collection).deleteOne({_id:o_id})
            .then(deletedItem => {
                if (deletedItem["deletedCount"]) {
                    return deletedItem["deletedCount"];
                } else {
                    throw new PrimaryKeyError();
                }
            });
    }
}
