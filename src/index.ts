import { app } from './app';
import {logger} from './common/logger.config';

app.listen(3000, () => logger.info('Server is running'));
