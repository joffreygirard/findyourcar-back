import { AbstractRouter } from '../common/abstract.router';
import { userController } from './user.controller';
import { IUser, IUserDto } from './user.model';
import { routeParamIdMiddleware } from '../middleware/route-param-id.middleware';
import { authenticationMiddleware } from '../middleware/authentication.middleware';

class UserRouter extends AbstractRouter<IUser, IUserDto> {
    protected controller = userController;

    protected configure() {
        this.router.get('/', (req, res, next) => this.controller.findAll(req, res, next));

        this.router.get('/:id',
            routeParamIdMiddleware,
            (req, res, next) => this.controller.get(req, res, next));

        this.router.post('/',
            (req, res, next) => this.controller.create(req, res, next));

        this.router.put('/:id',
            routeParamIdMiddleware,
            authenticationMiddleware,
            (req, res, next) => this.controller.update(req, res, next));

        this.router.delete('/:id',
            routeParamIdMiddleware,
            authenticationMiddleware,
            (req, res, next) => this.controller.remove(req, res, next));
    }

}

export const userRouter = new UserRouter().router;
