import { NextFunction, Request, Response } from 'express';
import { ErrorType, IAppError } from '../common/error/error.model';

export const routeParamIdMiddleware = (request: Request, response: Response, next: NextFunction): void => {
    const id = request.params.id;
    if (id.length != 24) {
        const error: IAppError = { type: ErrorType.resourceIdFormat }
        next(error);
    } else {
        next();
    }
}
